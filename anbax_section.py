#
# Copyright (C) 2018 Marco Morandini
#
#----------------------------------------------------------------------
#
#    This file is part of Anba.
#
#    Anba is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Hanba is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
#
#----------------------------------------------------------------------
#

import dolfin
# from dolfin import compile_extension_module
import numpy as np
from petsc4py import PETSc

import anba4
import mshr

import meshio
import matplotlib.pyplot as plt

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.

E = 1.
nu = 0.25
#Assmble into material mechanical property Matrix.
matMechanicProp = [E, nu]
# Meshing domain.


lx = 0.2
ly = 0.25

if 0:
    section = mshr.Rectangle(dolfin.Point(-lx/2, -ly/2, 0.), dolfin.Point(lx/2, ly/2, 0.))
    mesh = mshr.generate_mesh(section, 4)

    dolfin.plot(mesh)
    plt.show()
else:
    med_mesh = meshio.read('Mesh_1.med','med')

    points = np.zeros((med_mesh.points.shape[0],3))
    points[:,:2] = med_mesh.points

    print(points)

    meshio.write("Mesh_1.xdmf", meshio.Mesh(points=med_mesh.points, cells={"quad": med_mesh.cells[0].data}))

    with dolfin.cpp.io.XDMFFile(dolfin.MPI.comm_world, 'Mesh_1.xdmf') as xdmf_infile:
        mesh = dolfin.cpp.mesh.Mesh()
        xdmf_infile.read(mesh)

    if True:
        dolfin.plot(mesh)
        plt.show()






# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())

materials.set_all(0)
fiber_orientations.set_all(0.0)
plane_orientations.set_all(90.0)

# Build material property library.
mat1 = anba4.material.IsotropicMaterial(matMechanicProp, 1.)

matLibrary = []
matLibrary.append(mat1)

anba = anba4.anbax(mesh, 2, matLibrary, materials, plane_orientations, fiber_orientations)
stiff = anba.compute()
stiff.view()

mass = anba.inertia()
mass.view()

print(mass[0,0])

stress_result_file = dolfin.XDMFFile('Stress.xdmf')
stress_result_file.parameters['functions_share_mesh'] = True
stress_result_file.parameters['rewrite_function_mesh'] = False
stress_result_file.parameters["flush_output"] = True

anba.stress_field([1., 0., 0.,], [0., 0., 0.], "local", "paraview")
anba.strain_field([1., 0., 0.,], [0., 0., 0.], "local", "paraview")
stress_result_file.write(anba.STRESS, t = 0.)
stress_result_file.write(anba.STRAIN, t = 1.)


