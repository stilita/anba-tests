import numpy as np
import matplotlib.pyplot as plt


def Nodes_C3D20(nx, ny, nz, lx, ly, lz):
    """
    function to compute nodes and C3D20 elements coordinates for a beam
     with rectangular base lx*ly and height lz

    input
    nx : number of elements in x direction
    ny : number of elements in y direction
    lx :  base x
    ly :  base y
    lz: height

    output
    - matrix of nodes coordinates
    - matrix of elements (which nodes belong to each element)
    - matrix of constrained elements
    - matrix of loaded elements
    """

    # compute the nodes on a x-y face
    nodes_on_face = (2 * nx + 1) * (ny + 1) + (nx + 1) * ny
    nodes_between_faces = (nx + 1) * (ny + 1)

    total_nodes = nodes_on_face * (nz + 1) + nodes_between_faces * nz

    nodes = np.zeros((total_nodes , 4))
    nodes[:, 0] = np.arange(1, total_nodes + 1)

    # dense lines
    x_dense  = list(np.linspace(-lx / 2., lx / 2., 2 * nx + 1))
    # sparse lines
    x_sparse = list(np.linspace(-lx / 2., lx / 2., nx + 1))

    x_dense_face = []
    for i_y in range(2 * ny + 1):
        if (i_y % 2) == 0:
            x_dense_face += x_dense
        else:
            x_dense_face += x_sparse

    x_sparse_face = []
    for i_x in range(ny + 1):
        x_sparse_face += x_sparse

    x_nodes = []
    for i_z in range(2 * nz + 1):
        if i_z % 2 == 0:
            x_nodes += x_dense_face
        else:
            x_nodes += x_sparse_face

    nodes[:, 1] = np.asarray(x_nodes)

    # y coordinate

    # dense lines
    y_dense = np.linspace(-ly / 2., ly / 2., 2 * ny + 1)
    y_dense_t  = np.tile(y_dense[0::2], ( 2 * nx + 1, 1))
    # sparse lines
    y_sparse_t = np.tile(y_dense[1::2], (nx +1 ,1))
    
    y_dense_face = []
    
    i_dense = 0
    i_sparse = 0
    
    for i_y in range(2 * ny +1):
        if (i_y % 2) == 0:
            y_dense_face += list(y_dense_t[:, i_dense])
            i_dense += 1
        else:
            y_dense_face += list(y_sparse_t[:, i_sparse])
            i_sparse += 1

    y_sparse_face = list(np.reshape(np.tile(np.linspace(-ly / 2., ly / 2., ny + 1), (nx +1, 1)),(nx +1)*(ny + 1),'F'))

    i_dense = 0
    i_sparse = 0

    #for i_y in range(2 * ny + 1):
    #    if (i_y % 2) == 0:
    #        y_dense_face += list(y_dense_t[:, i_dense])
    #        i_dense += 1
    #    else:
    #        y_dense_face += list(y_sparse_t[:, i_sparse])
    #        i_sparse += 1

    y_nodes = []
    for i_z in range(2 * nz + 1):
        if i_z % 2 == 0:
            y_nodes += y_dense_face
        else:
            y_nodes += y_sparse_face

    nodes[:, 2] = np.asarray(y_nodes)

    z_dense = np.linspace(0., lz, 2 * nz + 1)

    z_nodes = []
    for i_z, curr_z in enumerate(z_dense):
        if i_z % 2 == 0:
            z_nodes += [curr_z]*((2 * nx + 1) * (ny + 1) + (nx + 1) * ny)
        else:
            z_nodes += [curr_z]*(nx + 1)*(ny + 1)

    nodes[:, 3] = np.asarray(z_nodes)

    if False:
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.scatter3D(nodes[:,1], nodes[:,2], nodes[:,3], c=nodes[:,1])

        plt.show()

    elements = np.zeros((nx * ny * nz, 21), dtype='int')

    elements[:, 0] = np.arange(1, nx * ny * nz + 1)  # elem number

    count = 0

    for i_z in range(nz):
        for i_y in range(ny):
            for i_x in range(1, 2*nx+1, 2):
                # number of nodes before current x-y plane
                face_start = i_z * ((2*nx+1)*(ny+1)+(nx+1)*ny+(nx+1)*(ny+1))
                # number of nodes before following x-y plane
                face_end   = (i_z + 1) * ((2*nx+1)*(ny+1)+(nx+1)*ny+(nx+1)*(ny+1))
                # number of nodes before the mid nodes
                face_mid = face_start + (2 * nx + 1) * (ny + 1) + (nx + 1) * ny
                row_start  = i_y * (3 * nx + 2)
                row_end = (i_y + 1) * (3 * nx + 2)
                # print(face_start, face_end, row_start, row_end)
                i_xh = (i_x+1)/2
                elements[count, 1:] = \
                    [face_start + row_start + i_x, face_start + row_start + i_x + 2,                      # nodes 1 2
                     face_start + row_end + i_x + 2, face_start + row_end + i_x,                          # nodes 3 4
                     face_end + row_start + i_x, face_end + row_start + i_x + 2,                          # nodes 5 6
                     face_end + row_end + i_x + 2, face_end + row_end + i_x,                              # nodes 7 8
                     face_start + row_start + i_x + 1, face_start + row_start + (2 * nx + 1) + i_xh + 1,  # nodes 9 10
                     face_start + row_end + i_x + 1, face_start + row_start + (2 * nx + 1) + i_xh,        # nodes 11 12
                     face_end + row_start + i_x + 1, face_end + row_start + (2 * nx + 1) + i_xh + 1,      # nodes 13 14
                     face_end + row_end + i_x + 1, face_end + row_start + (2 * nx + 1) + i_xh,            # nodes 15 16
                     face_mid + i_xh + i_y * (nx + 1), face_mid + i_xh + i_y * (nx + 1) + 1,              # nodes 17 18
                     face_mid + i_xh + (i_y + 1) * (nx + 1) + 1, face_mid + i_xh + (i_y + 1) * (nx + 1)]  # nodes 19 20
                count += 1

    full_face_nodes = (2 * nx + 1) * (ny + 1) + (nx + 1) * ny
    mid_face_nodes = (nx + 1) * (ny + 1)

    R1_nodes = np.arange(1, full_face_nodes + 1, dtype='int')

    R2_nodes = np.arange(nz * (full_face_nodes + mid_face_nodes) + 1, nodes.shape[0] + 1, dtype='int')

    return nodes, elements, R1_nodes, R2_nodes


def Nodes_C3D8(nx, ny, nz, lx, ly, lz):
    """
    function to compute nodes and C3D8 elements coordinates for a beam
     with rectangular base lx*ly and height lz

    input
    nx : number of elements in x direction
    ny : number of elements in y direction
    lx :  base x
    ly :  base y
    lz: height

    output
    - matrix of nodes coordinates
    - matrix of elements (which nodes belong to each element)
    - matrix of constrained elements
    - matrix of loaded elements
    """
    nodes = np.zeros(((nx + 1) * (ny + 1) * (nz + 1), 4))

    nodes[:, 0] = np.arange(1, (nx + 1) * (ny + 1) * (nz + 1) + 1)
    nodes[:, 1] = np.tile(np.linspace(-lx / 2., lx / 2., nx + 1), (ny + 1) * (nz + 1))

    # list of y coords
    y_coords = np.linspace(-ly / 2., ly / 2., ny + 1)
    # repeat for each x-y plane
    xy_plane = np.tile(y_coords, ((nx + 1), 1))
    # reshape into an array
    xy = xy_plane.reshape((nx + 1) * (ny + 1), order='F')
    # tile nz + 1 times
    nodes[:, 2] = np.tile(xy, nz + 1)

    # z coordinates
    nodes[:, 3] = np.tile(np.linspace(0, lz, nz + 1), ((nx + 1) * (ny + 1), 1)).reshape((nx + 1) * (ny + 1) * (nz + 1),
                                                                                        order='F')
    # z coordinate is 0

    elements = np.zeros((nx * ny * nz, 9), dtype='int')

    elements[:, 0] = np.arange(1, nx * ny * nz + 1) # elem number

    count = 0

    for i_z in range(nz):
        for i_y in range(ny):
            for i_x in range(1, nx+1):
                # number of nodes before current x-y plane
                face_start = i_z*(nx+1)*(ny+1)
                # number of nodes before following x-y plane
                face_end   = (i_z+1)*(nx+1)*(ny+1)
                row_start  = i_y * (nx+1)
                elements[count,1:] = [face_end+row_start+i_x+1, face_end+row_start+i_x, face_end+row_start+i_x+nx+1, face_end+row_start+i_x+nx+2,
                                      face_start+row_start+i_x+1, face_start+row_start+i_x, face_start+row_start+i_x+nx+1, face_start+row_start+i_x+nx+2]
                #elements[count,1:] = [face_start+row_start+i_x, face_start+row_start+i_x+1, face_start+row_start+i_x+nx+1, face_start+row_start+i_x+nx+2,
                #                      face_end+row_start+i_x, face_end+row_start+i_x+1, face_end+row_start+i_x+nx+1, face_end+row_start+i_x+nx+2]
                count += 1

    R1_nodes = np.arange(1, (nx + 1) * (ny + 1) + 1, dtype='int')

    R2_nodes = np.arange((nx + 1) * (ny + 1) * nz + 1, (nx + 1) * (ny + 1) * (nz +1) + 1, dtype='int')

    return nodes, elements, R1_nodes, R2_nodes


def write_inp_file(lx, ly, lz, nx, ny, nz, eltype, matname, E, nu, rho, abaqus=False, outputformat='MATRIX INPUT'):

    if eltype == 'C3D8':
        n_coords, n_elems, R1, R2 = Nodes_C3D8(nx, ny, nz, lx, ly, lz)
    elif eltype == 'C3D20':
        n_coords, n_elems, R1, R2 = Nodes_C3D20(nx, ny, nz, lx, ly, lz)
    else:
        print('Element type not implemented. Exiting..')
        exit(-1)

    basename = eltype + '_' + str(nx).zfill(2) + 'x_' + str(ny).zfill(2) + 'y_'+ str(nz).zfill(2) + 'z'

    if abaqus:
        basename += '_ABQ'
    else:
        basename += '_CCX'

    filename = basename + '.inp'

    outfile = open(filename, "wt")

    outfile.write("** M K matrices input file\n")

    # NODES section

    outfile.write("**\n")
    outfile.write("** Nodes\n")
    outfile.write("**\n")
    outfile.write("*NODE, nset = Nall\n")

    for i in range(n_coords.shape[0]):
        nodestring = "{0:4d},{1:16.6f},{2:16.6f},{3:16.6f}\n".format(int(n_coords[i,0]), *n_coords[i,1:])
        outfile.write(nodestring)

    # ELEMENTS section

    outfile.write("**\n")
    outfile.write("** Elements\n")
    outfile.write("**\n")
    outfile.write("*ELEMENT, TYPE={0}, ELSET=Eall\n".format(eltype))
    for i in range(n_elems.shape[0]):
        for i_el, n_el in enumerate(n_elems[i,:]):
            if i_el < len(n_elems[i,:])-1:
                outfile.write("{0:4d},".format(n_el))
                if i_el == 10:
                    outfile.write("\n\t ")
            else:
                outfile.write("{0:4d}\n".format(n_el))

    # R1 and R2 node sets
    outfile.write("**\n")
    outfile.write("** R1 and R2 sets\n")
    outfile.write("**\n")

    outfile.write("*NSET, nset = R1\n")
    for i_R, n_R in enumerate(R1):
        if i_R < len(R1)-1:
            outfile.write("{0:4d},".format(n_R))
            if i_R % 16 == 15:
                outfile.write("\n")
        else:
            outfile.write("{0:4d}\n".format(n_R))

    outfile.write("*NSET, nset = R2\n")
    for i_R, n_R in enumerate(R2):
        if i_R < len(R2)-1:
            outfile.write("{0:4d},".format(n_R))
            if i_R % 16 == 15:
                outfile.write("\n")
        else:
            outfile.write("{0:4d}\n".format(n_R))


    # MATERIAL section

    outfile.write("**\n")
    outfile.write("** Materials\n")
    outfile.write("**\n")
    outfile.write("*MATERIAL, name = {0}\n".format(matname))
    outfile.write("*ELASTIC\n")
    outfile.write("{0},{1:6f}\n".format(E, nu))
    outfile.write("*DENSITY\n")
    outfile.write("{0:f}\n".format(rho))



    # SOLID section

    outfile.write("**\n")
    outfile.write("** Solid section\n")
    outfile.write("**\n")
    outfile.write("*SOLID SECTION, MATERIAL={0}, ELSET={1}\n".format(matname, "Eall"))

    # BOUNDARY CONDITIONS

    #outfile.write("**\n")
    #outfile.write("** Boundary conditions\n")
    #outfile.write("**\n")
    #outfile.write("*BOUNDARY\n")
    #outfile.write("1,\t1,\t3\n")
    #outfile.write("{0},\t1\n".format(100))
    #outfile.write("{0},\t3\n".format(100))

    # calculation steps

    outfile.write("**\n")
    outfile.write("** Step\n")
    outfile.write("**\n")

    outfile.write("*STEP, name=MK\n")

    if abaqus:
        # from the abaqus docs:
        # Set FORMAT=MATRIX INPUT (default) to specify that the output use the matrix input text format that is consistent with the format used by the matrix definition technique in Abaqus/Standard
        # Set FORMAT=LABELS to specify that the output use the standard labeling format.
        # Set FORMAT=COORDINATE to specify that the output use the common mathematical coordinate format.
        # Set FORMAT=DMIG to specify that the output use the NASTRAN DMIG-s format.
        # Set FORMAT=USER ELEMENT to specify the output format for use under the MATRIX option for symmetric or unsymmetric linear user elements.
        outfile.write("*MATRIX GENERATE, STIFFNESS, MASS\n")
        outfile.write("*MATRIX OUTPUT, STIFFNESS, MASS, FORMAT = {0}\n".format(outputformat))
    else:
        outfile.write("*FREQUENCY, SOLVER=MATRIXSTORAGE\n")

    outfile.write("*END STEP\n\n")

    outfile.close()


write_inp_file(0.2, 0.25, 0.5, 1, 1, 1, 'C3D20', matname='dummy', E=1.0, nu=0.25, rho=1.0, abaqus=False, outputformat='MATRIX INPUT')

