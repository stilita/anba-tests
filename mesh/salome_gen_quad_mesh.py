#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.9.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/ANBA_test')

####################################################
##       Begin of NoteBook variables section      ##
####################################################
notebook.set("x1", -0.1)
notebook.set("x2", 0.1)
notebook.set("y1", -0.125)
notebook.set("y2", 0.125)
####################################################
##        End of NoteBook variables section       ##
####################################################
###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
Vertex_1 = geompy.MakeVertex("x1", "y1", 0)
Vertex_2 = geompy.MakeVertex("x1", "y2", 0)
Vertex_3 = geompy.MakeVertex("x2", "y2", 0)
Vertex_4 = geompy.MakeVertex("x2", "y1", 0)
Line_1 = geompy.MakeLineTwoPnt(Vertex_1, Vertex_4)
Line_2 = geompy.MakeLineTwoPnt(Vertex_4, Vertex_3)
Line_3 = geompy.MakeLineTwoPnt(Vertex_3, Vertex_2)
Line_4 = geompy.MakeLineTwoPnt(Vertex_2, Vertex_1)
Wire_1 = geompy.MakeWire([Line_1, Line_2, Line_3, Line_4], 1e-07)
Face_1 = geompy.MakeFaceWires([Wire_1], 1)
lx = geompy.CreateGroup(Face_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(lx, [3, 8])
ly = geompy.CreateGroup(Face_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(ly, [10, 6])
[lx, ly] = geompy.GetExistingSubObjects(Face_1, False)
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Vertex_1, 'Vertex_1' )
geompy.addToStudy( Vertex_2, 'Vertex_2' )
geompy.addToStudy( Vertex_3, 'Vertex_3' )
geompy.addToStudy( Vertex_4, 'Vertex_4' )
geompy.addToStudy( Line_1, 'Line_1' )
geompy.addToStudy( Line_2, 'Line_2' )
geompy.addToStudy( Line_3, 'Line_3' )
geompy.addToStudy( Line_4, 'Line_4' )
geompy.addToStudy( Wire_1, 'Wire_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudyInFather( Face_1, lx, 'lx' )
geompy.addToStudyInFather( Face_1, ly, 'ly' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

Mesh_1 = smesh.Mesh(Face_1,'Mesh_1')
Quadrangle_2D = Mesh_1.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,-1,[],[])
lx_1 = Mesh_1.GroupOnGeom(lx,'lx',SMESH.EDGE)
ly_1 = Mesh_1.GroupOnGeom(ly,'ly',SMESH.EDGE)
Regular_1D = Mesh_1.Segment(geom=ly)
ny = Regular_1D.NumberOfSegments(2)
[ lx_1, ly_1 ] = Mesh_1.GetGroups()
Regular_1D_1 = Mesh_1.Segment(geom=lx)
nx = Regular_1D_1.NumberOfSegments(2)
isDone = Mesh_1.Compute()
[ lx_1, ly_1 ] = Mesh_1.GetGroups()
smesh.SetName(Mesh_1, 'Mesh_1')
try:
  Mesh_1.ExportMED( r'/home/claudio/Projects/Studies/BeamProperties/ANBA_test/Mesh_1.med', 0, 41, 1, Mesh_1, 1, [], '',-1, 1 )
  pass
except:
  print('ExportPartToMED() failed. Invalid file name?')
Sub_mesh_1 = Regular_1D.GetSubMesh()
Sub_mesh_2 = Regular_1D_1.GetSubMesh()


## Set names of Mesh objects
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(nx, 'nx')
smesh.SetName(ny, 'ny')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(lx_1, 'lx')
smesh.SetName(ly_1, 'ly')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
