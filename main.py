import matplotlib.pyplot as plt
import numpy as np

import anba_rect_mesh as aq

# material properties
E= 1.
nu = 0.25
rho = 1.

# geometric properties
lx = 0.2
ly = 0.25

# computed quantities:
G = E / (2 * (1 + nu))

A = lx * ly
Jx = 1. / 12 * lx * ly ** 3
Jy = 1. / 12 * ly * lx ** 3

# Roark formula for polar inertia table 10.7 pag. 401
if ly > lx:
    a = ly/2.
    b = lx/2.
else:
    a = lx/2.
    b = ly/2.

Jz = a*b**3*(16./3 - 3.36*b/a*(1-b**4/(12*a**4)))

EA_th = E*A
EAPS_th = E*(1-nu)/((1+nu)*(1-2*nu))*A
GAx_th = 5/6*G*A
GAy_th = 5/6*G*A

EJx_th = E*Jx
EJy_th = E*Jy
GJz_th = G*Jz


#"print(\"axial     EA : theoretical  {0:.3e} - computed {1:.3e}\".format(E*A,D[0,0]))\n",
#"print(\"axial     EA : plain strain {0:.3e} - computed {1:.3e}\".format(E*(1-ν)/((1+ν)*(1-2*ν))*A,D[0,0]))\n",
#"print(\"shear     GA : theoretical  {0:.3e} - computed {1:.3e}\".format(5/6*G*A,D[1,1]))\n",
#"print(\"shear     GA : theoretical  {0:.3e} - computed {1:.3e}\".format(5/6*G*A,D[2,2]))\n",
#"print(\"torsional GJx: theoretical  {0:.3e} - computed {1:.3e}\".format(G*Jx,D[3,3]))\n",
#"print(\"bending   EJy: theoretical  {0:.3e} - computed {1:.3e}\".format(E*Jy,D[4,4]))\n",
#"print(\"bending   EJz: theoretical  {0:.3e} - computed {1:.3e}\".format(E*Jz,D[5,5]))"


num = np.arange(0,6)

num_cells_x = 2**(num)
num_cells_y = num_cells_x
degree = [1, 2, 3]

EA_c = {}
GAx_c = {}
GAy_c = {}
EJx_c = {}
EJy_c = {}
GJz_c = {}

for deg in degree:

    print("**** DEGREE: {0} ***".format(deg))

    EA_c[deg]  = []
    GAx_c[deg] = []
    GAy_c[deg] = []
    EJx_c[deg] = []
    EJy_c[deg] = []
    GJz_c[deg] = []

    for ncx in num_cells_x:
        if deg < 3 or ncx < 64:
            data = aq.anba_quad(ncx, ncx, deg, E, nu, rho, lx, ly, quad=False)
            m, cm, J = aq.compute_mass_props(data["mass"])
            #print(m)
            #print(cm)
            #print(J)
            EA_c[deg].append(data["stiff"][2,2])
            GAx_c[deg].append(data["stiff"][0, 0])
            GAy_c[deg].append(data["stiff"][1, 1])
            EJx_c[deg].append(data["stiff"][3, 3])
            EJy_c[deg].append(data["stiff"][4, 4])
            GJz_c[deg].append(data["stiff"][5, 5])
            print("degree: {0}".format(deg))
            print("num cells: {0}".format(ncx))
            print(data["stiff"])


plt.figure(figsize=(16,24))


plt.subplot(321)
for deg in degree:
    l = len(EA_c[deg])
    plt.plot(num[:l], EA_c[deg], label=r'deg. {0}'.format(deg))
plt.plot([num[0], num[-1]],[EA_th, EA_th], ls=':', label='theoretical')
plt.xlabel(r'$\log_2$ elems on side')
plt.ylim([EA_th-0.2, EA_th+0.2])
plt.ylabel(r'EA')
plt.legend()
plt.grid()

plt.subplot(322)
for deg in degree:
    l = len(GJz_c[deg])
    plt.plot(num[:l], GJz_c[deg], label=r'deg. {0}'.format(deg))
plt.plot([num[0], num[-1]],[GJz_th, GJz_th], ls=':', label='theoretical')
plt.xlabel(r'$\log_2$ elems on side')
plt.ylabel(r'$GJ_z$')
plt.legend()
plt.grid()

plt.subplot(323)
for deg in degree:
    l = len(GAx_c[deg])
    plt.plot(num[:l], GAx_c[deg], label=r'deg. {0}'.format(deg))
plt.plot([num[0], num[-1]],[GAx_th, GAx_th], ls=':', label='theoretical')
plt.xlabel(r'$\log_2$ elems on side')
plt.ylabel(r'$GA_x$')
plt.legend()
plt.grid()

plt.subplot(324)
for deg in degree:
    l = len(GAy_c[deg])
    plt.plot(num[:l], GAy_c[deg], label=r'deg. {0}'.format(deg))
plt.plot([num[0], num[-1]],[GAy_th, GAy_th], ls=':', label='theoretical')
plt.xlabel(r'$\log_2$ elems on side')
plt.ylabel(r'$GA_y$')
plt.legend()
plt.grid()

plt.subplot(325)
for deg in degree:
    l = len(EJx_c[deg])
    plt.plot(num[:l], EJx_c[deg], label=r'deg. {0}'.format(deg))
plt.plot([num[0], num[-1]],[EJx_th, EJx_th], ls=':', label='theoretical')
plt.xlabel(r'$\log_2$ elems on side')
plt.ylabel(r'$EJ_x$')
plt.legend()
plt.grid()

plt.subplot(326)
for deg in degree:
    l = len(EJy_c[deg])
    plt.plot(num[:l], EJy_c[deg], label=r'deg. {0}'.format(deg))
plt.plot([num[0], num[-1]],[EJy_th, EJy_th], ls=':', label='theoretical')
plt.xlabel(r'$\log_2$ elems on side')
plt.ylabel(r'$EJ_y$')
plt.legend()
plt.grid()

plt.suptitle(r'Rectangle section {0}$\times${1} ANBA'.format(lx,ly))

plt.savefig('ANBA.png')
plt.show()

print(GAx_c[3])

if __name__ == "__main__":
    pass

