import dolfin
import numpy as np
#import matplotlib.pyplot as plt

import anba4

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.
E = 1.
nu = 0.25
#Assmble into material mechanical property Matrix.
matMechanicProp = [E, nu]

# Meshing domain.
mesh = dolfin.Mesh()
e = dolfin.MeshEditor()

# Parameters:
# mesh – The mesh to open.
# type (std::string) – Cell type.
# tdim (std::size_t) – The topological dimension.
# gdim (std::size_t) – The geometrical dimension.
# degree (std::size_t) – The polynomial degree.
e.open(mesh, "quadrilateral", 2, 3, degree=1)

# rectangle size
lx = 0.2
ly = 0.25

# number of cells x
num_cells_x = 2

# number of cells y
num_cells_y = 2

dx = np.linspace(-lx/2, lx/2, num_cells_x+1)
dy = np.linspace(-ly/2, ly/2, num_cells_y+1)

# number of vertices
e.init_vertices(dx.size * dy.size)
#number of cells
e.init_cells(num_cells_x * num_cells_y)

#add vertices
count = 0

for curr_y in dy:
    for curr_x in dx:
        e.add_vertex(count, dolfin.Point(curr_x, curr_y, 0.))
        count += 1

#add cells
count = 0

for iy in range(num_cells_y):
    for ix in range(num_cells_x):
        e.add_cell(count, [(num_cells_x+1)*iy + ix, (num_cells_x+1)*iy + ix + 1,
                         (num_cells_x+1)*(iy+1) + ix, (num_cells_x+1)*(iy+1) + ix + 1])
        count += 1

#e.add_cell(0, [0, 1, 3, 4])
#e.add_cell(1, [4, 5, 1, 2])  #bad
#e.add_cell(1, [1, 2, 4, 5])  # good
e.close()

# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())

materials.set_all(0)
fiber_orientations.set_all(0.0)
plane_orientations.set_all(90.0)

# Build material property library.
mat1 = anba4.material.IsotropicMaterial(matMechanicProp, 1.)

matLibrary = []
matLibrary.append(mat1)

degree = 2
anba = anba4.anbax(mesh, degree, matLibrary, materials, plane_orientations, fiber_orientations)
stiff = anba.compute()
#stiff.view()

mass = anba.inertia()
mass.view()

print(mass[0,0])
