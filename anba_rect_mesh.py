import dolfin
import numpy as np
import matplotlib.pyplot as plt

import anba4

dolfin.parameters["form_compiler"]["optimize"] = True



def compute_mass_props(mmat):
    lin_mass = mmat[0, 0]
    S_cm = np.zeros((3, 1))
    S_cm[0] = (mmat[5, 1] - mmat[4, 2]) / 2
    S_cm[1] = (mmat[3, 2] - mmat[5, 0]) / 2
    S_cm[2] = (mmat[4, 0] - mmat[3, 1]) / 2
    p_cm = S_cm / mmat[0, 0]
    Jcm = mmat[3:6, 3:6] - mmat[0, 0] * np.eye(3) * (p_cm.T.dot(p_cm) - np.outer(p_cm, p_cm))

    return lin_mass, p_cm, Jcm


def quadrangle_mesh(lx, ly, num_cells_x, num_cells_y):
    # Meshing domain.
    mesh = dolfin.Mesh()
    e = dolfin.MeshEditor()

    # Parameters:
    # mesh – The mesh to open.
    # type (std::string) – Cell type.
    # tdim (std::size_t) – The topological dimension.
    # gdim (std::size_t) – The geometrical dimension.
    # degree (std::size_t) – The polynomial degree.
    e.open(mesh, "quadrilateral", 2, 2, degree=1)

    #
    dx = np.linspace(-lx / 2, lx / 2, num_cells_x + 1)
    dy = np.linspace(-ly / 2, ly / 2, num_cells_y + 1)

    # number of vertices
    e.init_vertices(dx.size * dy.size)
    # number of cells
    e.init_cells(num_cells_x * num_cells_y)

    # add vertices
    count = 0

    for curr_y in dy:
        for curr_x in dx:
            e.add_vertex(count, dolfin.Point(curr_x, curr_y))
            count += 1

    # add cells
    count = 0

    for iy in range(num_cells_y):
        for ix in range(num_cells_x):
            e.add_cell(count, [(num_cells_x + 1) * iy + ix, (num_cells_x + 1) * iy + ix + 1,
                               (num_cells_x + 1) * (iy + 1) + ix, (num_cells_x + 1) * (iy + 1) + ix + 1])
            count += 1

    # e.add_cell(0, [0, 1, 3, 4])
    # e.add_cell(1, [4, 5, 1, 2])  #bad
    # e.add_cell(1, [1, 2, 4, 5])  # good
    e.close()

    return mesh


def triangle_mesh(lx, ly, num):
    import mshr

    rect = mshr.Rectangle(dolfin.Point(-lx/2., -ly/2, 0.), dolfin.Point(lx/2, ly/2, 0.))
    mesh = mshr.generate_mesh(rect, num)
    return mesh


def anba_quad(num_cells_x, num_cells_y, degree, E, nu, rho, lx, ly, quad=False):
    '''

    Generates quadrilateral mesh of a rectangular domain and computes stiffness and mass properties with anba

    :param num_cells_x: number of cells in x direction
    :param num_cells_y: number of cells in y direction
    :param degree:      degree of elements

    :param E:   Young's modulus (default 1.0)
    :param nu:  Poisson ratio (default 0.25)
    :param rho: material density (default 1.0)
    :param lx:  rectangle size in x direction (default 0.2)
    :param ly:  rectangle size in x direction (default 0.2)
    :return:
    '''
    # Assmble into material mechanical property Matrix.

    dolfin.parameters["form_compiler"]["quadrature_degree"] = max(2, degree)

    matMechanicProp = [E, nu]

    if quad:
        mesh = quadrangle_mesh(lx, ly, num_cells_x, num_cells_y)
    else:
        mesh = triangle_mesh(lx, ly, num_cells_x)

    #dolfin.plot(mesh)
    #plt.show()

    # CompiledSubDomain
    materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
    fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
    plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())

    materials.set_all(0)
    fiber_orientations.set_all(0.0)
    plane_orientations.set_all(90.0)

    # Build material property library.
    mat1 = anba4.material.IsotropicMaterial(matMechanicProp, rho)

    matLibrary = []
    matLibrary.append(mat1)

    anba = anba4.anbax(mesh, degree, matLibrary, materials, plane_orientations, fiber_orientations)
    stiff = anba.compute()
    # stiff.view()

    mass = anba.inertia()
    stiff1 = stiff.getDenseArray()

    # print(foil_stiff)

    mass1 = mass.getDenseArray()

    # need to copy otherwise I get garbage
    f1 = np.copy(stiff1)
    m1 = np.copy(mass1)

    # np.savetxt('mass1.txt', mass1, header='mass')

    mesh_data = {'stiff': f1, 'mass': m1}

    return mesh_data